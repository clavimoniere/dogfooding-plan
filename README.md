# Chad's Todos

A test project created to dogfood Plan features for my todo items

[Go to the board](https://gitlab.com/clavimoniere/to-do/-/boards)

Add
- [Add a weekly agenda](https://gitlab.com/clavimoniere/to-do/-/issues/new?issuable_template=agenda)
- [Add todo](https://gitlab.com/clavimoniere/to-do/-/issues/new)
